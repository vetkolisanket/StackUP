package com.example.sanket.stackup.adapters;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sanket.stackup.R;
import com.example.sanket.stackup.activities.MainActivity;
import com.example.sanket.stackup.common.StackUPApplication;
import com.example.sanket.stackup.models.StackQuestionResponse;
import com.example.sanket.stackup.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

/**
 * Created by sanket on 4/17/2016.
 */
public class StackQuestionAdapter extends RecyclerView.Adapter<StackQuestionAdapter.StackQuestionViewHolder>  {

    List<StackQuestionResponse> mStackQuestionResponseList;

    private static Typeface mFontAwesome = Typeface.createFromAsset(StackUPApplication.APP_CONTEXT.getAssets(), "fontawesome.ttf");

    public StackQuestionAdapter(List<StackQuestionResponse> stackQuestionResponseList) {
        mStackQuestionResponseList = stackQuestionResponseList;
    }

    @Override
    public StackQuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new StackQuestionViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_main, parent, false));
    }

    @Override
    public void onBindViewHolder(StackQuestionViewHolder holder, int position) {
        StackQuestionResponse stackQuestionResponse = mStackQuestionResponseList.get(position);

        if(holder.ivProfileImage.getTag() == null
                || !holder.ivProfileImage.getTag().toString().equals(stackQuestionResponse.owner.profileImage)) {
            String profileImageUrl = stackQuestionResponse.owner.profileImage;
            Picasso.with(holder.ivProfileImage.getContext())
                    .load(TextUtils.isEmpty(profileImageUrl) ? null : profileImageUrl)
                    .fit()
                    .centerCrop()
                    .into(holder.ivProfileImage);
            holder.ivProfileImage.setTag(profileImageUrl);
        }

        holder.tvQuestion.setText(stackQuestionResponse.title);

        StringBuilder stringBuilder = new StringBuilder();
        int size = stackQuestionResponse.tags.size();
        for (int i = 0; i < size; i++) {
            stringBuilder.append(" ").append(stackQuestionResponse.tags.get(i));
        }

        holder.tvTags.setText(String.format("%s %s", holder.tvTags.getContext().getString(R.string.tags), stringBuilder.toString()));
        holder.tvDisplayName.setText(stackQuestionResponse.owner.displayName);
        holder.tvCount.setText(String.format(Locale.getDefault(), "%d %s", stackQuestionResponse.score, holder.tvCount.getContext().getString(R.string.up)));
        holder.tvTime.setText(Utils.getTimePassed(stackQuestionResponse.lastActivityDate*1000));
    }

    @Override
    public int getItemCount() {
        return mStackQuestionResponseList.size();
    }

    public static class StackQuestionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView ivProfileImage;
        TextView tvQuestion;
        TextView tvTags;
        TextView tvDisplayName;
        TextView tvLike;
        TextView tvLink;
        TextView tvCount;
        TextView tvTime;

        public StackQuestionViewHolder(View itemView) {
            super(itemView);

            ivProfileImage = (ImageView) itemView.findViewById(R.id.iv_profile_image);
            tvQuestion = (TextView) itemView.findViewById(R.id.tv_question);
            tvTags = (TextView) itemView.findViewById(R.id.tv_tags);
            tvDisplayName = (TextView) itemView.findViewById(R.id.tv_display_name);
            tvLike = (TextView) itemView.findViewById(R.id.tv_like);
            tvLink = (TextView) itemView.findViewById(R.id.tv_link);
            tvCount = (TextView) itemView.findViewById(R.id.tv_count);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);

            tvLike.setTypeface(mFontAwesome);
            tvLink.setTypeface(mFontAwesome);
            tvCount.setTypeface(mFontAwesome);
            tvTags.setTypeface(mFontAwesome);

            tvLike.setText(tvLike.getContext().getString(R.string.like_o));
            tvLink.setText(tvLink.getContext().getString(R.string.link));

            tvLike.setOnClickListener(this);
            tvLink.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tv_like:
                    ((MainActivity)v.getContext()).saveQuestion(getAdapterPosition());
                    break;
                case R.id.tv_link:
                    ((MainActivity)v.getContext()).viewLink(getAdapterPosition());
                    break;
            }
        }
    }

}
