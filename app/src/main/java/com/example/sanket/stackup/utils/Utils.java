package com.example.sanket.stackup.utils;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by sanket on 4/17/2016.
 */
public class Utils {


    public static final String TIME_FORMAT = "hh:mma";
    public static final String MESSAGE_TIME_FORMAT = "dd MMM yy, hh:mma";

    public static String getTimePassed(long timeStamp) {
        Long currentTime = System.currentTimeMillis();
        Long difference = currentTime - timeStamp;
        if (difference < (60 * 1000)) {
            return "few seconds ago";
        } else if (difference < (60 * 60 * 1000)) {
            int minuteDifference = (int) (difference / (60 * 1000));
            return minuteDifference == 1 ? minuteDifference + " minute ago" : minuteDifference + " minutes ago";
        } else if (difference < (24 * 60 * 60 * 1000)) {
            int hourDifference = (int) (difference / (60 * 60 * 1000));
            return hourDifference == 1 ? hourDifference + " hour ago" : hourDifference + " hours ago";
        } else if (difference < (48 * 60 * 60 * 1000)) {
            return "Yesterday, " + getTime(timeStamp, TIME_FORMAT);
        }

        return getDate(timeStamp, MESSAGE_TIME_FORMAT);
    }

    public static String getTime(long timestamp, String format) {
        Date time = new Date(timestamp);
        SimpleDateFormat timeFormatter = new SimpleDateFormat(format, Locale.getDefault());
        DateFormatSymbols symbols = new DateFormatSymbols(Locale.getDefault());
        symbols.setAmPmStrings(new String[]{"am", "pm"});
        timeFormatter.setDateFormatSymbols(symbols);
        return timeFormatter.format(time);
    }

    public static String getDate(long timestamp, String format) {
        Date date = new Date(timestamp);
        SimpleDateFormat dateFormatter = new SimpleDateFormat(format, Locale.getDefault());
        DateFormatSymbols symbols = new DateFormatSymbols(Locale.getDefault());
        symbols.setAmPmStrings(new String[]{"am", "pm"});
        dateFormatter.setDateFormatSymbols(symbols);
        return dateFormatter.format(date);
    }

}
