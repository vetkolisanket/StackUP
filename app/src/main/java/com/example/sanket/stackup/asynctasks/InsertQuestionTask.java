package com.example.sanket.stackup.asynctasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.sanket.stackup.database.DatabaseManager;
import com.example.sanket.stackup.models.StackQuestionResponse;

import java.lang.ref.WeakReference;

/**
 * Created by sanket on 4/17/2016.
 */
public class InsertQuestionTask extends AsyncTask<StackQuestionResponse, Void, Boolean> {

    private final WeakReference<Context> mContextWeakReference;

    public InsertQuestionTask(Context context) {
        mContextWeakReference = new WeakReference<>(context);
    }


    @Override
    protected Boolean doInBackground(StackQuestionResponse... params) {
        Context context = mContextWeakReference.get();
        if (context != null) {
            DatabaseManager databaseManager = DatabaseManager.getInstance();
            StackQuestionResponse stackQuestionResponse = params[0];
            return databaseManager.insertQuestion(stackQuestionResponse);
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        Context context = mContextWeakReference.get();
        if(context != null) {
            if (result != null && result) {
                Toast.makeText(context, "Questions saved successfully", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Error while saving question!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
