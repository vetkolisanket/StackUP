package com.example.sanket.stackup.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.example.sanket.stackup.common.StackUPApplication;
import com.example.sanket.stackup.models.StackQuestionResponse;

/**
 * Created by sanket on 4/17/2016.
 */
public class DatabaseManager extends SQLiteOpenHelper {

    private static DatabaseManager mDatabaseManager = null;

    public static DatabaseManager getInstance() {
        if (mDatabaseManager == null) {
            mDatabaseManager = new DatabaseManager(StackUPApplication.APP_CONTEXT,
                    StackUPContract.DB_NAME, null, StackUPContract.DB_VERSION);
        }
        return mDatabaseManager;
    }

    private DatabaseManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_STACKQUESTION = "CREATE TABLE " + StackUPContract.StackQuestion.TABLE_NAME + " ( " +
                StackUPContract.StackQuestion.COLUMN_QUESTION_ID + " TEXT PRIMARY KEY, " +
                StackUPContract.StackQuestion.COLUMN_TAGS + " TEXT, " +
                StackUPContract.StackQuestion.COLUMN_OWNER_PIC + " TEXT, " +
                StackUPContract.StackQuestion.COLUMN_OWNER_NAME + " TEXT, " +
                StackUPContract.StackQuestion.COLUMN_SCORE + " INTEGER, " +
                StackUPContract.StackQuestion.COLUMN_LAST_ACTIVITY_DATE + " INTEGER, " +
                StackUPContract.StackQuestion.COLUMN_LINK + " TEXT, " +
                StackUPContract.StackQuestion.COLUMN_TITLE + " TEXT )";

        db.execSQL(CREATE_TABLE_STACKQUESTION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //in case of DB update
    }

    public boolean isQuestionPresentInDB(String questionId) {
        String Query = "SELECT * FROM " +
                StackUPContract.StackQuestion.TABLE_NAME +
                " WHERE " + StackUPContract.StackQuestion.COLUMN_QUESTION_ID + " = '" + questionId + "'";
        Cursor cursor = getReadableDatabase().rawQuery(Query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public boolean insertQuestion(StackQuestionResponse stackQuestionResponse) {
        if (!isQuestionPresentInDB(stackQuestionResponse.questionId)) {
            String tags = stackQuestionResponse.tags.toString().replace("[", "").replace("]", "");
            String INSERT_INTO_STACKQUESTION = "INSERT INTO " + StackUPContract.StackQuestion.TABLE_NAME + " ( " +
                    StackUPContract.StackQuestion.COLUMN_QUESTION_ID + "," +
                    StackUPContract.StackQuestion.COLUMN_TAGS + "," +
                    StackUPContract.StackQuestion.COLUMN_OWNER_PIC + "," +
                    StackUPContract.StackQuestion.COLUMN_OWNER_NAME + "," +
                    StackUPContract.StackQuestion.COLUMN_SCORE + "," +
                    StackUPContract.StackQuestion.COLUMN_LAST_ACTIVITY_DATE + "," +
                    StackUPContract.StackQuestion.COLUMN_LINK + "," +
                    StackUPContract.StackQuestion.COLUMN_TITLE + " ) VALUES(?,?,?,?,?,?,?,?)";
            SQLiteDatabase database = getReadableDatabase();
            SQLiteStatement statement = database.compileStatement(INSERT_INTO_STACKQUESTION);
            statement.bindString(1, stackQuestionResponse.questionId);
            statement.bindString(2, tags);
            statement.bindString(3, stackQuestionResponse.owner.profileImage);
            statement.bindString(4, stackQuestionResponse.owner.displayName);
            statement.bindLong(5, stackQuestionResponse.score);
            statement.bindLong(6, stackQuestionResponse.lastActivityDate);
            statement.bindString(7, stackQuestionResponse.link);
            statement.bindString(8, stackQuestionResponse.title);
            boolean result = statement.executeInsert() != -1;
            statement.close();
            return result;
        }
        return true;
    }

    public Cursor getSavedLikes() {
        String SELECT_KEYVALUE = "SELECT * FROM " + StackUPContract.StackQuestion.TABLE_NAME;
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(SELECT_KEYVALUE, null);
    }
}
