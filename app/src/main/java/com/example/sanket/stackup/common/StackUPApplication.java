package com.example.sanket.stackup.common;

import android.app.Application;
import android.content.Context;

/**
 * Created by sanket on 4/17/2016.
 */
public class StackUPApplication extends Application {

    public static Context APP_CONTEXT;

    @Override
    public void onCreate() {
        super.onCreate();

        APP_CONTEXT = this;
    }
}
