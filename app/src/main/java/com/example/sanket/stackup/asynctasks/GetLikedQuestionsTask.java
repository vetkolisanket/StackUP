package com.example.sanket.stackup.asynctasks;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import com.example.sanket.stackup.activities.MainActivity;
import com.example.sanket.stackup.database.DatabaseManager;
import com.example.sanket.stackup.database.StackUPContract;
import com.example.sanket.stackup.models.Owner;
import com.example.sanket.stackup.models.StackQuestionResponse;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sanket on 4/17/2016.
 */
public class GetLikedQuestionsTask extends AsyncTask<Void, Void, List<StackQuestionResponse>> {

    final WeakReference<Context> mContextWeakReference;

    public GetLikedQuestionsTask(Context context) {
        mContextWeakReference = new WeakReference<>(context);
    }

    @Override
    protected List<StackQuestionResponse> doInBackground(Void... params) {
        Context context = mContextWeakReference.get();
        List<StackQuestionResponse> stackQuestionResponseList = new ArrayList<>();
        if(context != null) {
            DatabaseManager databaseManager = DatabaseManager.getInstance();
            Cursor cursor = databaseManager.getSavedLikes();
            while (cursor.moveToNext()) {
                StackQuestionResponse stackQuestionResponse = new StackQuestionResponse();

                stackQuestionResponse.questionId = cursor.getString(cursor.getColumnIndex(StackUPContract.StackQuestion.COLUMN_QUESTION_ID));
                String tags = cursor.getString(cursor.getColumnIndex(StackUPContract.StackQuestion.COLUMN_TAGS));
                String [] arrTags = tags.split(",");
                stackQuestionResponse.tags = Arrays.asList(arrTags);
                String profileImageUrl = cursor.getString(cursor.getColumnIndex(StackUPContract.StackQuestion.COLUMN_OWNER_PIC));
                String displayName = cursor.getString(cursor.getColumnIndex(StackUPContract.StackQuestion.COLUMN_OWNER_NAME));
                stackQuestionResponse.owner = new Owner();
                stackQuestionResponse.owner.displayName = displayName;
                stackQuestionResponse.owner.profileImage = profileImageUrl;
                stackQuestionResponse.score = cursor.getInt(cursor.getColumnIndex(StackUPContract.StackQuestion.COLUMN_SCORE));
                stackQuestionResponse.lastActivityDate = cursor.getLong(cursor.getColumnIndex(StackUPContract.StackQuestion.COLUMN_LAST_ACTIVITY_DATE));
                stackQuestionResponse.link = cursor.getString(cursor.getColumnIndex(StackUPContract.StackQuestion.COLUMN_LINK));
                stackQuestionResponse.title = cursor.getString(cursor.getColumnIndex(StackUPContract.StackQuestion.COLUMN_TITLE));

                stackQuestionResponseList.add(stackQuestionResponse);
            }
            cursor.close();
        }
        return stackQuestionResponseList;
    }

    @Override
    protected void onPostExecute(List<StackQuestionResponse> stackQuestionResponses) {
        super.onPostExecute(stackQuestionResponses);

        Context context = mContextWeakReference.get();
        if (context != null) {
            ((MainActivity)context).updateList(stackQuestionResponses);
        }
    }
}
