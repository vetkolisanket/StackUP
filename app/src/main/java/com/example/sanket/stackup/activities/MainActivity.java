package com.example.sanket.stackup.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sanket.stackup.R;
import com.example.sanket.stackup.adapters.StackQuestionAdapter;
import com.example.sanket.stackup.asynctasks.GetLikedQuestionsTask;
import com.example.sanket.stackup.asynctasks.InsertQuestionTask;
import com.example.sanket.stackup.comparators.TimeComparator;
import com.example.sanket.stackup.comparators.VoteComparator;
import com.example.sanket.stackup.models.Items;
import com.example.sanket.stackup.models.StackQuestionResponse;
import com.example.sanket.stackup.retrofit.ApiClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private static final String BASE_URL = " https://api.stackexchange.com";

    private static final String TAG = ".MainActivity";
    private static String mTag = "";
    private static boolean isLikedBeingShown = false;

    private static List<StackQuestionResponse> mStackQuestionResponseList;
    private StackQuestionAdapter mStackQuestionAdapter;

    private ProgressDialog mPDMainActivity;
    private TextView mTVTitleSearch;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTVTitleSearch = (TextView) findViewById(R.id.tv_title_search);
        RecyclerView rvQuestionList = (RecyclerView) findViewById(R.id.rv_question_list);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.srl_questions);
        mPDMainActivity = new ProgressDialog(this);
        mPDMainActivity.setCancelable(false);
        mPDMainActivity.setMessage("Please wait");
        mPDMainActivity.setIndeterminate(true);

        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_dark,
                android.R.color.holo_red_light);

        //set listeners
        mSwipeRefreshLayout.setOnRefreshListener(this);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mStackQuestionResponseList = new ArrayList<>();
        mStackQuestionAdapter = new StackQuestionAdapter(mStackQuestionResponseList);

        if (rvQuestionList != null) {
            rvQuestionList.setLayoutManager(linearLayoutManager);
            rvQuestionList.setAdapter(mStackQuestionAdapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        final SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setIconifiedByDefault(true);
        searchView.setQueryHint("Search by tag");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String tag = query.trim();
                if(!TextUtils.isEmpty(tag)) {
                    mPDMainActivity.show();
                    mTag = tag;
                    fetchQuestions(tag);
                    isLikedBeingShown = false;
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_sort:
                if(item.getTitle().equals(getString(R.string.sort_by_votes))) {
                    Collections.sort(mStackQuestionResponseList, new VoteComparator());
                    item.setTitle(R.string.sort_by_time);
                } else {
                    Collections.sort(mStackQuestionResponseList, new TimeComparator());
                    item.setTitle(R.string.sort_by_votes);
                }
                mStackQuestionAdapter.notifyDataSetChanged();
                break;
            case R.id.menu_show_liked:
                new GetLikedQuestionsTask(this).execute();
                isLikedBeingShown = true;
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void fetchQuestions(final String tag) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .build();

        ApiClient apiClient = restAdapter.create(ApiClient.class);
        apiClient.getTaggedQuestions("desc", "activity", "False", 0, tag,
                "stackoverflow", new Callback<Items>() {
            @Override
            public void success(Items items, Response response) {
                Log.d(TAG, "success: " + items.toString());
                int quotaRemaining = items.quotaRemaining;
                int quotaMax = items.quotaMax;
                float percentQuotaRemaining = ((float) quotaMax - quotaRemaining)/quotaMax*100;
                if(items.stackQuestionResponseList.size() > 0) {
                    String prefixString = "Search result for ";
                    int startIndex = prefixString.length();

                    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(prefixString + tag + " tag.");
                    spannableStringBuilder.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), startIndex, startIndex + tag.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                    mTVTitleSearch.setText(spannableStringBuilder);
                    mStackQuestionResponseList.clear();
                    mStackQuestionResponseList.addAll(items.stackQuestionResponseList);
                    mStackQuestionAdapter.notifyDataSetChanged();
                    Toast.makeText(MainActivity.this, "API Quota: " + String.format(Locale.getDefault(), "%.0f", percentQuotaRemaining) + "%", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "No results found!", Toast.LENGTH_SHORT).show();
                }
                mPDMainActivity.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "failure: " + error.toString());
                mPDMainActivity.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void viewLink(int position) {
        StackQuestionResponse stackQuestionResponse = mStackQuestionResponseList.get(position);
        String url = stackQuestionResponse.link;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void saveQuestion(int position) {
        StackQuestionResponse stackQuestionResponse = mStackQuestionResponseList.get(position);
        new InsertQuestionTask(this).execute(stackQuestionResponse);
    }

    public void updateList(List<StackQuestionResponse> stackQuestionResponseList) {
        mStackQuestionResponseList.clear();
        mStackQuestionResponseList.addAll(stackQuestionResponseList);
        mStackQuestionAdapter.notifyDataSetChanged();
        mTVTitleSearch.setText(getString(R.string.liked_questions));
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        if(isLikedBeingShown) {
            new GetLikedQuestionsTask(this).execute();
        } else {
            fetchQuestions(mTag);
        }
    }
}
