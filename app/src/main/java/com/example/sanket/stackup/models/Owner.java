package com.example.sanket.stackup.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sanket on 4/17/2016.
 */
public class Owner {

    @SerializedName("profile_image")
    public String profileImage;

    @SerializedName("display_name")
    public String displayName;

}
