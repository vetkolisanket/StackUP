package com.example.sanket.stackup.database;

/**
 * Created by sanket on 4/17/2016.
 */
public final class StackUPContract {

    private StackUPContract() {
    }

    public static final String DB_NAME = "StackUP.db";

    public static final int DB_VERSION = 1;

    public static class StackQuestion {
        public static final String TABLE_NAME = "stackQuestion";

        public static final String COLUMN_QUESTION_ID = "questionId";

        public static final String COLUMN_TAGS = "tags";

        public static final String COLUMN_OWNER_PIC = "profileImageUrl";

        public static final String COLUMN_OWNER_NAME = "displayName";

        public static final String COLUMN_SCORE = "score";

        public static final String COLUMN_LAST_ACTIVITY_DATE = "lastActivityDate";

        public static final String COLUMN_LINK = "link";

        public static final String COLUMN_TITLE = "title";
    }

}
