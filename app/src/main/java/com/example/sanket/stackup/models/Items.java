package com.example.sanket.stackup.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sanket on 4/17/2016.
 */
public class Items {

    @SerializedName("items")
    public List<StackQuestionResponse> stackQuestionResponseList;

    @SerializedName("quota_max")
    public int quotaMax;

    @SerializedName("quota_remaining")
    public int quotaRemaining;
}
