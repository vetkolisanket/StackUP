package com.example.sanket.stackup.retrofit;

import com.example.sanket.stackup.models.Items;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by sanket on 4/17/2016.
 */
public interface ApiClient {

    @GET("/2.2/search/advanced")
    void getTaggedQuestions(@Query("order") String order, @Query("sort") String sort,
                                             @Query("accepted") String accepted, @Query("answers")
                                             int answers, @Query("tagged") String tag, @Query("site")
                                             String site, Callback<Items> callback);

}
