package com.example.sanket.stackup.comparators;

import com.example.sanket.stackup.models.StackQuestionResponse;

import java.util.Comparator;

/**
 * Created by sanket on 4/17/2016.
 */
public class VoteComparator implements Comparator<StackQuestionResponse> {
    @Override
    public int compare(StackQuestionResponse lhs, StackQuestionResponse rhs) {
        if(lhs.score > rhs.score) {
            return -1;
        } else if(lhs.score < rhs.score) {
            return 1;
        }
        return 0;
    }
}
