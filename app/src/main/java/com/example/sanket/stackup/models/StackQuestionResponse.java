package com.example.sanket.stackup.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sanket on 4/17/2016.
 */
public class StackQuestionResponse {

    @SerializedName("question_id")
    public String questionId;

    public List<String> tags;

    public Owner owner;

    public int score;

    @SerializedName("last_activity_date")
    public long lastActivityDate;

    public String link;

    public String title;

}
